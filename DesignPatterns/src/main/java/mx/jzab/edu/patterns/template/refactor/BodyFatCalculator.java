package mx.jzab.edu.patterns.template.refactor;

/**
 *
 * @author jzab
 */
public class BodyFatCalculator {

  public double calculate( BodyMeasures body, boolean isWoman ) {
    double fatPercent = 0;
    if( isWoman ) {
      fatPercent = Math.log( body.getWaist() + body.getHip() ) - body.getWeight()
                                                                 * Math.log( body.getHeight() );
    }
    else {
      fatPercent = Math.log( body.getWaist() + body.getAbdomen() ) - body.getWeight() * Math.
              log( body.getHeight() );
    }
    return fatPercent;
  }

}
