package mx.jzab.edu.patterns.abstractFactory.example;

import mx.jzab.edu.patterns.abstractFactory.RandomGenerator;

/**
 *
 * @author jzab
 */
public abstract class RandomNumberGeneratorFactory {

  public abstract RandomGenerator create( int type );

}
