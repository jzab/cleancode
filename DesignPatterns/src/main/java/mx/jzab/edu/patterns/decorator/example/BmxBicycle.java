
package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class BmxBicycle extends BicycleDecorator {

  public BmxBicycle( Bicycle bike ) {
    super( bike );
    if(bike instanceof BmxBicycle )
      throw new IllegalArgumentException("");
  }

  @Override
  public double getCost() {
    return bike.getCost() + 100;
  }


  public void jump() {
    System.out.println( "Jump " );
  }

}
