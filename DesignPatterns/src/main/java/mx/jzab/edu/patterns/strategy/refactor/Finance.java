package mx.jzab.edu.patterns.strategy.refactor;

import java.util.Date;

/**
 *
 * @author zjaramil
 */
public class Finance {

  double getCompanyIncomeForMonth( Date date )
  {
    return 999000000;
  }

  double getComissionFor( Date date, SalesMan salesMan )
  {
    return Math.max( Math.random() * salesMan.hashCode(), 40000 );
  }


}
