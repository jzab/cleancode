package mx.jzab.edu.patterns.decorator.refactor;

/**
 *
 * @author zjaramil
 */
public class MessageTransformerImpl implements MessageTransformer {

  private String message;
  private boolean inHtml;
  private boolean encryption;

  public MessageTransformerImpl( String message )
  {
    this.message = message;
  }

  public MessageTransformerImpl( String message, boolean inHtml )
  {
    this.message = message;
    this.inHtml = inHtml;
  }

  public MessageTransformerImpl( String message, boolean inHtml, boolean encryption )
  {
    this.message = message;
    this.inHtml = inHtml;
    this.encryption = encryption;
  }


  @Override
  public String transform()
  {
    if( encryption ) {
      message = message.replaceAll( "a", "A" )
            .replaceAll( "e", "E" )
            .replaceAll( "i", "I" )
            .replaceAll( "o", "O" )
            .replaceAll( "u", "U" );
    }
    if( inHtml ) {
      return "<html><body>" + message + "</body></html>";
    }
    return message;
  }

}
