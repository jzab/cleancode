/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.patterns.pool;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {

    DBConnectionPool pool = new DBConnectionPool();

    for( int i = 0; i < 8; i++ ) {
      Query query = new Query( "Thread" + i, pool );
      query.start();
      System.out.println( "Starting Thread" + i );
    }

  }

  private static class Query extends Thread {

    private DBConnectionPool pool;

    private Query( String name, DBConnectionPool pool ) {
      this.pool = pool;
      setName( name );
    }

    @Override
    public void run() {
      while( true ) {
        String connection = null;
        try {
          connection = pool.get();
          Thread.sleep( (long) ( Math.random() * 5000 ) );
        }
        catch( InterruptedException ex ) {
          Logger.getLogger( Main.class.getName() ).log( Level.SEVERE, null, ex );
        }
        finally {
          if( connection != null ) {
            pool.release( connection );
          }
        }

        try {
          Thread.sleep( 2000 );
        }
        catch( InterruptedException ex ) {
          Logger.getLogger( Main.class.getName() ).log( Level.SEVERE, null, ex );
        }
      }
    }

  }

}
