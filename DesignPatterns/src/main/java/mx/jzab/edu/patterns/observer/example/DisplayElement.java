package mx.jzab.edu.patterns.observer.example;

public interface DisplayElement {
	public void display();
}
