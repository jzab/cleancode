package mx.jzab.edu.patterns.template.example;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    Sandwich italian = new ItalianSandwich();
    italian.makeSandwich();

    System.out.println();
    System.out.println();

    Sandwich veggie = new VeggieSandwich();
    veggie.makeSandwich();

  }

}
