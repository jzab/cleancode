package mx.jzab.edu.patterns.abstractFactory;

import mx.jzab.edu.patterns.abstractFactory.example.RandomGeneratorConfig;

/**
 *
 * @author jzab
 */
public class MixedRandomGenerator extends RandomGenerator {

  private RandomGenerator gen1;
  private RandomGenerator gen2;

  public MixedRandomGenerator( RandomGeneratorConfig config,
                               RandomGenerator gen1, RandomGenerator gen ) {
    super( config );
    this.gen1 = gen1;
    this.gen2 = gen2;
  }

  @Override
  public int nextNumber() {
    return ( gen1.nextNumber() + gen2.nextNumber() ) % 10;
  }

}
