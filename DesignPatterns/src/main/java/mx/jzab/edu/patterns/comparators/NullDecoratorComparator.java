package mx.jzab.edu.patterns.comparators;

import java.util.Comparator;

/**
 *
 * @author jzab
 */
public class NullDecoratorComparator<T> implements Comparator<T> {

  private Comparator<T> comparator;
  private int sign = 1;

  public NullDecoratorComparator( Comparator<T> baseComparator ) {
    this.comparator = baseComparator;
  }

  public NullDecoratorComparator( Comparator<T> baseComparator, boolean nullFirst ) {
    this.comparator = baseComparator;
    this.sign = nullFirst ? 1 : -1;
  }

  @Override
  public int compare( T o1, T o2 ) {
    int compare;
    if( o1 == null && o2 == null ) {
      compare = 0;
    }
    else if( o1 == null ) {
      compare = -1 * sign;
    }
    else if( o2 == null ) {
      compare = 1 * sign;
    }
    else {
      compare = comparator.compare( o1, o2 );
    }
    return compare;
  }

}
