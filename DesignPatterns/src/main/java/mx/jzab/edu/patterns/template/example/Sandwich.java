package mx.jzab.edu.patterns.template.example;

/**
 *
 * @author jzab
 */
public abstract class Sandwich {

  public void makeSandwich() {
    addBread();
    if( wantMeat() ) {
      addMeat();
    }
    if( wantVeggies() ) {
      addVeggies();
    }
    addCondiments();
    finishSandwich();
  }

  protected void addBread() {
    System.out.println( "Add bread" );
  }

  protected boolean wantMeat() {
    return true;
  }

  protected abstract void addMeat();

  protected boolean wantVeggies() {
    return true;
  }

  protected void addVeggies() {
    System.out.println( "Add Tomato" );
    System.out.println( "Add Lettuce" );
  }

  protected void addCondiments() {
    System.out.println( "Add Salt and Peper" );
  }

  protected void finishSandwich() {
    System.out.println( "Sandwich finished" );
  }


}
