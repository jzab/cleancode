package mx.jzab.edu.patterns.builder.example;

/**
 *
 * @author jzab
 */
public class HonoluluComboBuilder extends HamburgerComboBuilder {

  @Override
  protected String getDrink() {
    return "Soda";
  }

  @Override
  protected String getFries() {
    return "Yes";
  }

  @Override
  protected String getTopping() {
    return "Pineapple";
  }

}
