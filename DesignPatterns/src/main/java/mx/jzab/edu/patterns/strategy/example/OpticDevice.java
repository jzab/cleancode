package mx.jzab.edu.patterns.strategy.example;

/**
 *
 * @author jzab
 */
public class OpticDevice extends SecurityDevice {

  public OpticDevice( Validation validation ) {
    super( validation );
  }

  @Override
  protected void openDoor() {
    System.out.println( "Opening door" );
  }

  @Override
  protected void sendDenyMessage() {
    System.out.println( "Displaying red light" );
  }

}
