package mx.jzab.edu.patterns.factory;

/**
 *
 * @author jzab
 */
public class MathRandomGenerator implements RandomGenerator {

  @Override
  public int nextNumber() {
    return (int) ( Math.random() * 10 );
  }

}
