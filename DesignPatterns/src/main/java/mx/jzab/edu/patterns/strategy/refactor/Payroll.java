/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.patterns.strategy.refactor;

import java.util.Date;

/**
 *
 * @author zjaramil
 */
public class Payroll {

  public double calculatePaymentFor( Employee e )
  {
    if( e instanceof Engineer ) {
      return ( (Engineer)e ).getSalary();
    }
    else if( e instanceof SalesMan ) {
      double commision = new Finance().getComissionFor( new Date(), (SalesMan)e );
      return ( (SalesMan)e ).getBaseSalary() + commision;
    }
    else if( e instanceof Ceo ) {
      double income = new Finance().getCompanyIncomeForMonth( new Date() );
      return income * ( (Ceo)e ).getBenefitPercent();
    }
    return Double.NaN;
  }

}
