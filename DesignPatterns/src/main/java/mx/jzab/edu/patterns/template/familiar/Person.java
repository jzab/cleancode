
package mx.jzab.edu.patterns.template.familiar;

/**
 *
 * @author jzab
 */
public class Person implements Comparable<Person> {

  private String name;

  public Person( String name ) {
    this.name = name;
  }

  @Override
  public int compareTo( Person o ) {
    int compare = 0;
    if( o instanceof Person ) {
      Person person = (Person) o;
      compare = this.name.compareTo( person.name );
    }
    return compare;
  }

  @Override
  public String toString() {
    return name;
  }


}
