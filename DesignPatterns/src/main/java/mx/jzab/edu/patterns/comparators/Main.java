package mx.jzab.edu.patterns.comparators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author jzab
 */
public class Main {

  private static final int SIZE = 3;

  public static void main( String[] args ) {
    //Normal comparator
    sortList( "Normal Comparator", new IntegerComparator() );

    //You use another object that already does the job
    sortList( "Composition Comparator", new IntegerCompositionDecorator( true ) );

    //Add features at runtime, maybe sort by Absolute value
    sortList( "Decorator Comparator",
              new NullDecoratorComparator<>( new IntegerComparator(), true ) );

    //Have a base class, that way you force all to implement the same base behavior
    sortList( "Template Comparator", new IntegerTemplateComparator( true ) );
    
  }

  private static void sortList( String title, Comparator<Integer> comparator ) {
    System.out.println( "******* " + title + " *******" );
    List<Integer> list = getList();
    printList( list );

    try {
      Collections.sort( list, comparator );
    }
    catch( NullPointerException e ) {
      System.out.println( "Error sorting" );
    }

    System.out.println( "--- Sorted ---" );
    printList( list );
  }

  private static List<Integer> getList() {
    List<Integer> list = new ArrayList<>( SIZE + 2 );
    for( int i = 0; i < SIZE; i++ ) {
      list.add( (int) ( Math.random() * 100 ) );
    }
    list.add( null );
    list.add( null );
    return list;
  }

  public static void printList( List<Integer> list ) {
    list.stream().forEach( System.out::println );
  }

}
