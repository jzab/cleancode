/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class TwoPeopleBicycle extends BicycleDecorator {

  public TwoPeopleBicycle(Bicycle bike){
    super(bike);
  }

}
