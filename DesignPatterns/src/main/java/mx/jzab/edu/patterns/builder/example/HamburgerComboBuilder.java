package mx.jzab.edu.patterns.builder.example;

/**
 *
 * @author jzab
 */
public abstract class HamburgerComboBuilder {

  public HamburgerComboBuilder() {
  }

  protected abstract String getDrink();

  protected String getMeat() {
    return "meat";
  }

  protected abstract String getFries();

  protected abstract String getTopping();

}
