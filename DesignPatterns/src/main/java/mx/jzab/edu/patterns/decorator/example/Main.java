package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    Bicycle basicBike = new BasicBicycle();
    print( "Basic", basicBike );

    Bicycle bike = new SlimWheelBicycle( basicBike );
    print( "Slim Wheel", bike );

    bike = new ProfesionalPedalsBicycle( basicBike );
    print( "Profesional Pedals", bike );

    bike = new ProfesionalPedalsBicycle( new SlimWheelBicycle( basicBike ) );
    print( "Pedals&SlimWheel", bike );

    Bicycle bmx = new SlimWheelBicycle( new BmxBicycle( new ProfesionalPedalsBicycle( basicBike ) ));
    Bicycle bmx2 = new BmxBicycle(new BmxBicycle( basicBike ));
    ((BmxBicycle)bmx2).jump();

    print( "Bmx", bmx2 );
  }

  private static void print( String name, Bicycle bike ) {
    System.out.println();
    System.out.println( name );
    System.out.print( "Cost: " );
    System.out.println( bike.getCost() );

    System.out.print( "Top Speed: " );
    System.out.println( bike.getTopSpeed() );

    bike.doBreak();
  }

}
