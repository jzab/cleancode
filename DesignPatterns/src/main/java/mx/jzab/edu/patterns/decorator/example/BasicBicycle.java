
package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class BasicBicycle implements Bicycle {

   @Override
   public int getTopSpeed() {
      return 10;
   }

   @Override
   public double getCost() {
      return 110.0;
   }

   @Override
   public void doBreak() {
      System.out.println( "Breaking.. (10 meters needed to break)" );
   }

}
