package mx.jzab.edu.patterns.factory;

/**
 *
 * @author jzab
 */
public interface RandomGenerator {

  int nextNumber();

}
