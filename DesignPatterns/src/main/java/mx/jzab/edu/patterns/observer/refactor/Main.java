package mx.jzab.edu.patterns.observer.refactor;

import java.util.Date;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {

    int secondsFlag = 0;
    while( true ) {
      int seconds = new Date().getSeconds();
      if( ( ( seconds % 5 ) == 0 ) && ( secondsFlag != seconds ) ) {
        System.out.println( "Five seconds passed:" + seconds );
        secondsFlag = seconds;
      }
    }

  }

}
