package mx.jzab.edu.patterns.strategy.example;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    Validation v = new DBValidation();
    SecurityDevice device1 = new OpticDevice( v );

    Validation v2 = new LDAPValidation();
    SecurityDevice device2 = new OpticDevice( v2 );

    User user = new User();
    device1.validate( user );
    device2.validate( user );

    device1.addValidation( new HolidayValidation() );
    device1.validate( user );

  }
}
