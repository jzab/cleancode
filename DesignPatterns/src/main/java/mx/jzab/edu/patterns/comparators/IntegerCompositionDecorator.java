package mx.jzab.edu.patterns.comparators;

import java.util.Comparator;

/**
 *
 * @author jzab
 */
public class IntegerCompositionDecorator implements Comparator<Integer> {

  private ObjectUtils util;

  public IntegerCompositionDecorator( boolean nullFirst ) {
    this.util = new ObjectUtils( nullFirst );
  }

  @Override
  public int compare( Integer o1, Integer o2 ) {
    int result;
    Integer nullCheck = util.compare( o1, o2 );
    if( nullCheck != null ) {
      result = nullCheck;
    }
    else {
      result = o1.compareTo( o2 );
    }
    return result;
  }

}
