package mx.jzab.edu.patterns.strategy.refactor;

import java.util.List;

/**
 *
 * @author zjaramil
 */
public class PayrollReport {


  public String getPerEmployeeReport( List<Employee> employees )
  {
    String report = "";
    for( Employee e : employees ) {
      report += new Payroll().calculatePaymentFor( e );
      report += "\n";
    }
    return report;
  }

  public String getPerDepartmentReport( List<Employee> employees )
  {
    return null;
  }

}
