package mx.jzab.edu.patterns.observer.familiar;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {

    Person man = new Person( 20, "Single" );
    Person woman = new Person( 18, "In a relationship" );

    man.addPropertyChangeListeners( new PropertyChangeListener() {
      @Override
      public void propertyChange( PropertyChangeEvent evt ) {
        System.out.println( evt.toString() );
      }
    } );

    woman.addPropertyChangeListeners( ( PropertyChangeEvent evt ) -> {
      if( Person.STATUS.equals( evt.getPropertyName() ) ) {
        if( "Single".equals( evt.getNewValue() ) ) {
          System.out.println( "Making my move" );
        }
        else {
          System.out.println( "Waiting" );
        }
      }
    } );

    man.setAge( 21 );

    woman.setStatus( "Heart Broken" );
    woman.setStatus( "Single" );

  }

}
