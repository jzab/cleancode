package mx.jzab.edu.patterns.singleton.example;

import javax.swing.Icon;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    Icon icon = IconDB.getInstance().getIconFor( "Run" );
    System.out.println( icon );
  }

}
