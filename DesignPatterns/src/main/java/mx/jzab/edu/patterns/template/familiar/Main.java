/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.patterns.template.familiar;

import java.util.Arrays;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    Person[] persons = new Person[5];
    persons[0] = new Person( "Zab" );
    persons[1] = new Person( "Bale" );
    persons[2] = new Person( "William" );
    persons[3] = new Person( "Juan" );
    persons[4] = new Person( "Peter" );

    Arrays.stream( persons )
            .forEach( System.out::println );

    Arrays.sort( persons );

    System.out.println();
    System.out.println( "Sorted" );

    Arrays.stream( persons )
            .forEach( System.out::println );

    Other[] others = new Other[3];
    others[0] = new Other( "Zab" );
    others[1] = new Other( "Bale" );
    others[1] = new Other( "X" );

    Arrays.sort( others );
  }


}
