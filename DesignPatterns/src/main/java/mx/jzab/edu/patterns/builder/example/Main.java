package mx.jzab.edu.patterns.builder.example;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main( String[] args ) {
    HamburgerComboBuilder builder = new HonoluluComboBuilder();
    HamburgerCombo combo = new HamburgerCombo( builder );

    System.out.println( combo );

    HamburgerComboBuilder customBuilder = new CustomComboBuilder()
            .setDrink( "Fanta" )
            .setFries( "No" )
            .setMeat( "Portobello" )
            .setTopping( "mayonnaise" );

    HamburgerCombo customCombo = new HamburgerCombo( customBuilder );
    System.out.println( customCombo );

  }

}
