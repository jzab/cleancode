
package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public class ProfesionalPedalsBicycle extends BicycleDecorator {

  public ProfesionalPedalsBicycle( Bicycle bike ) {
    super( bike );
  }

  @Override
  public double getCost() {
    return bike.getCost() + 30;
  }

}
