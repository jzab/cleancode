package mx.jzab.edu.patterns.factory;

import java.util.Date;

/**
 *
 * @author jzab
 */
public class DateRandomGenerator implements RandomGenerator {

  @Override
  public int nextNumber() {
    return Math.abs( (int) new Date().getTime() ) % 10;
  }

}
