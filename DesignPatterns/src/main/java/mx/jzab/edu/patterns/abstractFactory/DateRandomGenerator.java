package mx.jzab.edu.patterns.abstractFactory;

import java.util.Date;
import mx.jzab.edu.patterns.abstractFactory.example.RandomGeneratorConfig;

/**
 *
 * @author jzab
 */
public class DateRandomGenerator extends RandomGenerator {

  public DateRandomGenerator( RandomGeneratorConfig config ) {
    super( config );
  }

  @Override
  public int nextNumber() {
    return Math.abs( (int) new Date().getTime() ) % 10;
  }

}
