package mx.jzab.edu.patterns.template.refactor;

/**
 *
 * @author jzab
 */
public class BodyMeasures {

  private double height;
  private double waist;
  private double weight;
  private double hip;
  private double abdomen;

  public BodyMeasures( double height, double waist, double weight, double hip, double abdomen ) {
    this.height = height;
    this.waist = waist;
    this.weight = weight;
    this.hip = hip;
    this.abdomen = abdomen;
  }

  /**
   * @return the height
   */
  public double getHeight() {
    return height;
  }

  /**
   * @return the waist
   */
  public double getWaist() {
    return waist;
  }

  /**
   * @return the weight
   */
  public double getWeight() {
    return weight;
  }

  /**
   * @return the hip
   */
  public double getHip() {
    return hip;
  }

  /**
   * @return the abdomen
   */
  public double getAbdomen() {
    return abdomen;
  }


}
