package mx.jzab.edu.patterns.abstractFactory.example;

import mx.jzab.edu.patterns.abstractFactory.DateRandomGenerator;
import mx.jzab.edu.patterns.abstractFactory.MathRandomGenerator;
import mx.jzab.edu.patterns.abstractFactory.MixedRandomGenerator;

/**
 *
 * @author jzab
 */
public class PositiveRandomNumberGeneratorFactory extends RandomNumberGeneratorFactory {

  public mx.jzab.edu.patterns.abstractFactory.RandomGenerator create( int type ) {
    RandomGeneratorConfig config = new PositiveRandomGeneratorConfig();
    mx.jzab.edu.patterns.abstractFactory.RandomGenerator random = null;
    switch( type ) {
      case 1:
        random = new DateRandomGenerator( config );
        break;
      case 2:
        random = new MathRandomGenerator( config );
        break;
      case 3:
        random = new MixedRandomGenerator( config,
                                           new DateRandomGenerator( config ),
                                           new MathRandomGenerator( config ) );
        break;
      default:
        throw new IllegalArgumentException( "No valid type" );
    }
    return random;
  }

}
