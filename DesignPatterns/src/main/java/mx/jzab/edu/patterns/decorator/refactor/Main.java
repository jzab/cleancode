package mx.jzab.edu.patterns.decorator.refactor;

/**
 *
 * @author zjaramil
 */
public class Main {

  public static void main( String[] args )
  {
    //I want now a transformer that converts to UpperCase and encypts.
    //I want now a transformer that converts to LowerCase and enclose in html.
    MessageTransformer transformer = new MessageTransformerImpl( "This is a long message" );
    System.out.println( transformer.transform() );

    transformer = new MessageTransformerImpl( "This is a long message", true );
    System.out.println( transformer.transform() );

    transformer = new MessageTransformerImpl( "This is a long message", true, true );
    System.out.println( transformer.transform() );

  }

}
