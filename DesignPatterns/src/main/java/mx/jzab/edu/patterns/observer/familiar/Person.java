package mx.jzab.edu.patterns.observer.familiar;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author jzab
 */
public class Person {

  public static final String STATUS = "STATUS";
  public static final String AGE = "AGE";

  private PropertyChangeSupport pcs;
  private int age;
  private String status;

  public Person( int age, String status ) {
    this.age = age;
    this.status = status;
    this.pcs = new PropertyChangeSupport( this );
  }

  public void setAge( int age ) {
    int oldAge = this.age;
    this.age = age;
    pcs.firePropertyChange( AGE, oldAge, age );
  }

  public void setStatus( String status ) {
    String oldStatus = this.status;
    this.status = status;
    pcs.firePropertyChange( STATUS, oldStatus, status );
  }

  public void addPropertyChangeListeners( PropertyChangeListener listener ) {
    this.pcs.addPropertyChangeListener( listener );
  }

  public void removePropertyChangeListener( PropertyChangeListener listener ) {
    this.pcs.removePropertyChangeListener( listener );
  }


}
