package mx.jzab.edu.patterns.strategy.refactor;

/**
 *
 * @author zjaramil
 */
public class Ceo extends Employee {

  public Ceo( String name, String department )
  {
    super( name, department );
  }

  double getBenefitPercent()
  {
    return .1;
  }

}
