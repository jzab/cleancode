package mx.jzab.edu.patterns.decorator.example;

/**
 *
 * @author jzab
 */
public interface Bicycle {

   public int getTopSpeed();

   public double getCost();

   public void doBreak();

}
