/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.patterns.comparators;

/**
 *
 * @author jzab
 */
public class ObjectUtils {

  private int sign;

  public ObjectUtils( boolean nullFirst ) {
    this.sign = nullFirst ? 1 : -1;
  }

  public Integer compare( Object o1, Object o2 ) {
    Integer compare;
    if( o1 == null && o2 == null ) {
      compare = 0;
    }
    else if( o1 == null ) {
      compare = -1 * sign;
    }
    else if( o2 == null ) {
      compare = 1 * sign;
    }
    else {
      compare = null;
    }
    return compare;
  }
}
