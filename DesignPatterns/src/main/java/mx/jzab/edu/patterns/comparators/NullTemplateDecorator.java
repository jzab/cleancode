package mx.jzab.edu.patterns.comparators;

import java.util.Comparator;

/**
 *
 * @author jzab
 * @param <T>
 */
public abstract class NullTemplateDecorator<T> implements Comparator<T> {

  private int sign;

  public NullTemplateDecorator( boolean nullFirst ) {
    this.sign = nullFirst ? 1 : -1;
  }

  @Override
  public int compare( T o1, T o2 ) {
    int compare;
    if( o1 == null && o2 == null ) {
      compare = 0;
    }
    else if( o1 == null ) {
      compare = -1 * sign;
    }
    else if( o2 == null ) {
      compare = 1 * sign;
    }
    else {
      compare = compareObjects( o1, o2 );
    }
    return compare;
  }

  public abstract int compareObjects( T o1, T o2 );


}
