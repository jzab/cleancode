package mx.jzab.edu.patterns.strategy.familiar;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.XMLFormatter;

/**
 *
 * @author jzab
 */
public class LoggerStrategy {

   public static void main( String[] args ) throws IOException {
      Logger logger = Logger.getLogger( "Logger" );
      logger.setUseParentHandlers( false );

      ConsoleHandler handler = new ConsoleHandler();
      logger.addHandler( handler );

      Formatter formatter = new SimpleFormatter();
      handler.setFormatter( formatter );

      logger.log( Level.INFO, "Simple Message" );

      Formatter xmlFormat = new XMLFormatter();
      handler.setFormatter( xmlFormat );

      logger.log( Level.INFO, "XML Message" );

   }

}
