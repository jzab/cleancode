package mx.jzab.edu.patterns.abstractFactory.example;

/**
 *
 * @author jzab
 */
public class PositiveRandomGeneratorConfig implements RandomGeneratorConfig {

  @Override
  public boolean isPositive() {
    return true;
  }

  @Override
  public int minNumber() {
    return 1;
  }

  @Override
  public int maxNumber() {
    return 50;
  }

}
