/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.tdd;

import junit.framework.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author L03054478
 */
public class ListTest {

  @Test
  public void testSize(){
    List<String> list = new MyList<>();
    int size = list.getSize();
    Assert.assertEquals(0, size);
  }
  
  @Test
  public void testSizeNotEmpty(){
    List<String> list = new MyList<>();
    list.add("Item");
    int size = list.getSize();
    Assert.assertEquals(1, size);
  }

  @Test
  public void testRemoveSize(){
    List<String> list = new MyList<>();
    list.add("Item");
    Assert.assertEquals(1, list.getSize());
    list.remove(0);
    Assert.assertEquals(0, list.getSize());
  }

  @Test
  public void testClear(){
    List<String> list = new MyList<>();
    list.add("Item");
    list.add("Item2");
    list.clear();
    Assert.assertEquals(0, list.getSize());
  }

  @Test
  public void testAdd(){
    List<String> list = new MyList<>();
    list.add("Item");
    Assert.assertNotNull(list.getElement(0));
  }

  @Test
  public void testAddValid(){
    List<String> list = new MyList<>();
    String toAdd = "Item";
    list.add(toAdd);
    Assert.assertEquals(toAdd, list.getElement(0));
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void testAddNull(){
    List<String> list = new MyList<>();
    list.add(null);
  }

 @Test(expectedExceptions = IllegalArgumentException.class)
  public void getInvalidIndex(){
    List<String> list = new MyList<>();
    list.getElement(15);
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void getInvalidIndex2(){
    List<String> list = new MyList<>();
    list.getElement(0);
  }


}
