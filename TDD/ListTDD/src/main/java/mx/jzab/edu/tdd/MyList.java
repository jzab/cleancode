/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.jzab.edu.tdd;

/**
 *
 * @author L03054478
 */
public class MyList<E> implements List<E>{
  int size;
  Object[] elements = new Object[500];

  @Override
  public int getSize(){
    return size; //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void add(E element){
    if(element == null){
      throw new IllegalArgumentException("Element cannot be null");
    }
    elements[size] = element;
    size++;
  }

  @Override
  public void remove(int index){
    this.size--;
  }
  public void clear(){
    this.size = 0;
  }

  public E getElement(int index){

    if(this.size <= index || this.size == 0){
      throw new IllegalArgumentException("Invalid index");
    }
    return (E) elements[index];
  }
}
