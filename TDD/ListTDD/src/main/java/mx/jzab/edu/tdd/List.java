package mx.jzab.edu.tdd;

/**
 *
 * @author jzab
 */
public interface List<E> {

  public int getSize();
  public void add(E element);
  public void remove(int index);
  public void clear();
  public E getElement(int index);

}
