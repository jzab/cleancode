package mx.jzab.edu.solid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author zjaramil
 */
public class NotSolidFinances {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws IOException{
    Scanner scanner = new Scanner(System.in);

    System.out.println("¿How many purchases did you did today?");

    int purchasesCount = Integer.parseInt(scanner.nextLine());

    String[] payees = new String[purchasesCount];
    double[] amounts = new double[purchasesCount];

    for (int i = 0; i < purchasesCount; i++){
      String line = scanner.nextLine();
      String[] parts = line.split(" ");

      payees[i] = parts[0];
      amounts[i] = Double.parseDouble(parts[1]);
    }

    Map<String, Integer> map = new HashMap<>();
    double min = Double.MAX_VALUE;
    double max = 0;
    double avg = 0;

    for (int i = 0; i < purchasesCount; i++){
      if (min > amounts[i]){
        min = amounts[i];
      }
      if (max < amounts[i]){
        max = amounts[i];
      }
      avg += amounts[i];

      Integer count = map.getOrDefault(payees[i], 0);
      map.put(payees[i], count + 1);
    }

    avg /= purchasesCount;

    Optional<Map.Entry<String, Integer>> frequent =
        map.entrySet().stream().max((e1, e2) -> e1.getValue().compareTo(e2.getValue()));

    try {
      //Dependency Inversion
      System.out.println("Purchases of " + getCurrentTime().toString());
    }
    catch (JSONException | ProtocolException | JAXBException ex) {
      Logger.getLogger(NotSolidFinances.class.getName()).log(Level.SEVERE, null, ex);
    }

    System.out.println("Min: " + min);
    System.out.println("Max: " + max);
    System.out.println("Avg: " + avg);

    System.out.println("Frequent Payee:" + frequent.get().getKey());

  }

  public static Date getCurrentTime() throws ProtocolException, JAXBException, IOException, JSONException{
    Date actualDate = new Date();
    HttpURLConnection connection = null;

    try {
      URL url = new URL("http://www.worldtimeserver.com/handlers/GetData.ashx?action=GCTData&_=1503378080754");
      connection = (HttpURLConnection)url.openConnection();
      connection.setRequestMethod("GET");
      connection.setRequestProperty("Accept", "application/json");

      InputStream inputStream = connection.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

      String jsonContent =
          "{\"LocIDDescription\":\"Mexico - Jalisco\",\"LocIDLocationID\":\"MX-JAL\",\"CountryCode\":\"MX\"," +
           "\"CountryName\":\"Mexico\",\"State\":\"Jalisco\",\"City\":\"Guadalajara\"," +
           "\"TimeZone\":\"America/Mexico_City\",\"DstZoneName\":\"Central Daylight Time\"," +
           "\"DstZoneAbbrev\":\"CDT\",\"StdZoneName\":\"Central Standard Time\",\"StdZoneAbbrev\":\"CST\"," +
           "\"IsInDST\":false,\"ThisTime\":\"2018-01-24T23:36:44.5874229Z\",\"DateTime_12HR\":\"11:36 PM\"," +
          " \"DateTime_24HR\":\"23:36:44\",\"FormattedDate\":\"Wednesday, January 24, 2018\",\"Lat\":20.6667," +
           "\"Lng\":-103.3333,\"ErrorMsg\":null,\"serverTimeStamp\":1516837004587.4229,\"dataFrom\":\"IP\"}";

//     JAXBContext jc = JAXBContext.newInstance(FosterHome.class);
//
//        Unmarshaller unmarshaller = jc.createUnmarshaller();
//        FosterHome fosterHome = (FosterHome) unmarshaller.unmarshal(new File("src/nov18/input.xml"));
//
//        Marshaller marshaller = jc.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//        marshaller.marshal(fosterHome, System.out);
//    }
      JSONObject jsonObject = new JSONObject(jsonContent);
      
      String thisTime = jsonObject.getString("ThisTime");
      System.out.println("CurrentTime: " + thisTime);
    }
    catch (IOException | JSONException ex) {
      Logger.getLogger(NotSolidFinances.class.getName()).log(Level.SEVERE, null, ex);
    }
    finally {
      if (connection != null){
        connection.disconnect();
      }
    }

    return actualDate;
  }

}
