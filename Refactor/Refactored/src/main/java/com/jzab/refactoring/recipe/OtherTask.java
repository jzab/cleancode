package com.jzab.refactoring.recipe;

/**
 *
 * @author zjaramil
 */
class OtherTask extends ITask {

  private boolean prepared;

  public void prepare(){
    System.out.println("Preparing for task");
    prepared = true;
  }

  public Object calculate(){
    if(prepared){
      return "Other";
    }
    else{
      throw new IllegalStateException("Task is not prepared");
    }
  }

  public void compute(Object calculation){
    System.out.println("Common execution code");
    System.out.printf("Execute %s task %n", calculation);
  }

  public void finish(){
    System.out.println("Finish task");
    System.out.println("------------------");
  }



}
