/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.constructors;


public class PersonBuilder {

  private int age;
  private String name;
  private String bloodType;
  private String gender;
  private String countryOfBirth;
  private String greeting;

  public PersonBuilder(){
  }

  public PersonBuilder setAge(int age){
    this.age = age;
    return this;
  }

  public PersonBuilder setName(String name){
    this.name = name;
    return this;
  }

  public PersonBuilder setBloodType(String bloodType){
    this.bloodType = bloodType;
    return this;
  }

  public PersonBuilder setGender(String gender){
    this.gender = gender;
    return this;
  }

  public PersonBuilder setCountryOfBirth(String countryOfBirth){
    this.countryOfBirth = countryOfBirth;
    return this;
  }

  public PersonBuilder setGreeting(String greeting){
    this.greeting = greeting;
    return this;
  }

  public Person createPerson(){
    assert age != 0;
    assert name != null;
    assert gender != null;
    return new Person(age, name, bloodType, gender, countryOfBirth, greeting);
  }

}
