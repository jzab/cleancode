package com.jzab.refactoring.changes;

/**
 *
 * @author jzab
 */
public class LinkedIn implements JobProcessor{

  public void process(Job job){
      System.out.println("[LinkedIn] Posting Job " + job.description);
  }

}
