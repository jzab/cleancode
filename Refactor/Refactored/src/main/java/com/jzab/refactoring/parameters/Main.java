package com.jzab.refactoring.parameters;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;

/**
 *
 * @author zjaramil
 */
public class Main {



  public static void main(String[] args) throws DataAccessException {
    ConnectionManager connection = null;
    Context context =  null;
    String viewName = "HR$V_EMPLOYEES";

    EmployeeDAO dao = new EmployeeDAO(connection, context, viewName);
    
    try {
      dao.getEmployees();
    }
    catch (DataAccessException ex) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }

    try {
      dao.getEmployees("Carlos");
    }
    catch (DataAccessException ex) {
      Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
    }


    dao.getEmployee(10);

    Employee e = new Employee();
    e.setName("Tony Stark");
    e.setDepartment("Enginnering");
    e.setLevel("M3");
    e.setManagerId(1);
    dao.insert(e);

    //dao.insert("PEter Parker", "Security", "IC2", 1);
  }

}
