package com.jzab.refactoring.parameters;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 * @author zjaramil
 */
public class EmployeeDAO {

  private final ConnectionManager manager;
  private final Context context;
  private final String viewName;

  public EmployeeDAO(ConnectionManager manager, Context context, String viewName){
    this.manager = manager;
    this.context = context;
    this.viewName = viewName;
  }

  public List<String> getEmployees() throws DataAccessException {
    getEnviroment("Something");
    //SOME SQL CODE
    return Collections.emptyList();
  }

  public List<String> getEmployees(String name) throws DataAccessException{
    getEnviroment("Other");
    //SOME SQL CODE
    return Collections.emptyList();
  }

  public String getEmployee(int id) throws DataAccessException{
    getEnviroment("Another");
    //SOME SQL CODE
    return "";
  }

  private void getEnviroment(String variable) throws DataAccessException{
    try {
      context.getEnvironment().get(variable);
    }
    catch (NamingException ex) {
      throw new DataAccessException("We must set the " + variable + " variable in the context", ex);
    }
  }

  public void insert(Employee e) throws DataAccessException{
    Connection connection = null;
    try{
      Object value = context.getEnvironment().get("Another");
      connection = manager.createConnection();
      PreparedStatement statement = connection.prepareStatement("Insert into ....");
      statement.setString(1, e.getName());
      statement.executeUpdate();
    }
    catch(SQLException | NamingException ex){
      throw new DataAccessException("Error inserting Employee", ex);
    }
    finally{
      if(connection != null){
        try {
          connection.close();
        }
        catch (SQLException ex) {
          Logger.getLogger(EmployeeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
  }

}
