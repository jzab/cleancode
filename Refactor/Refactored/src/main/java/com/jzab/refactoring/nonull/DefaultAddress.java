package com.jzab.refactoring.nonull;

import com.jzab.refactoring.nonull.Address;

/**
 *
 * @author jzab
 */
public class DefaultAddress extends Address {

  public DefaultAddress(){
    super("No Address");
  }

  @Override
  public String coordinates(){
    return "No coordinates";
  }







}
