/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.changes;

/**
 *
 * @author jzab
 */
public interface JobProcessor {

  public void process(Job job);

}
