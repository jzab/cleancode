package com.jzab.refactoring.versions;

/**
 *
 * @author zjaramil
 */
public class WindowsController extends AbstractController {

  @Override
  public void doSomething(){
    System.out.println("Windows doSomethig");
  }

  @Override
  public void doAnotherThing(){
    System.out.println("Windows doAnotherThing");
  }

  

}
