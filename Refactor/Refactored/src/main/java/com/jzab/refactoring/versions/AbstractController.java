package com.jzab.refactoring.versions;

/**
 *
 * @author jzab
 */
public abstract class AbstractController implements ControllerInterface {

  @Override
  public void doSame(){
    System.out.println("SameTask");
  }


  public static ControllerInterface create(){
    ControllerInterface controller = null;
    if(System.getProperty("OS").equals("MAC")){
      controller = new MacController();
    }
    else{
      controller = new WindowsController();
    }
    return controller;
  }

}
