package com.jzab.refactoring.constants;

/**
 *
 * @author zjaramil
 */
public interface Event {

  public void process();

}
