package com.jzab.refactoring.constructors;

/**
 *
 * @author jzab
 */
public class Main {

  public static void main(String[] args){
    copyPerson();
  }

  private static void copyPerson(){
    Person person = new PersonBuilder()
        .setAge(18)
        .setName("Juan")
        .setBloodType("AB")
        .setGender("M")
        .setCountryOfBirth("MX")
        .setGreeting("Hola").createPerson();

    Person mexican = Person.createMexican(18, "Juan", "AB", "M", "Hola");


    //Person twin = new Person(person);
    person.salutation();
    //twin.salutation();

  }

  private static void copyConstructor(){
    PersonCopyConstructor person = new PersonCopyConstructor(18, "Juan", "AB", "M", "MX", "Programmer");
    PersonCopyConstructor twin = new PersonCopyConstructor(person);
  }

}
