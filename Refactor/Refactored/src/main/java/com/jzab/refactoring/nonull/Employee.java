
package com.jzab.refactoring.nonull;

/**
 *
 * @author jzab
 */
public class Employee {

  private Address homeAddress;
  private Address workAddress;

  public Employee(){
    this.homeAddress = new DefaultAddress();
    this.workAddress = new DefaultAddress();
  }

  public Address getHomeAddress(){
    return homeAddress;
  }

  public void setHomeAddress(Address homeAddress){
    this.homeAddress = homeAddress;
  }

  public Address getWorkAddress(){
    if(workAddress == null){
      workAddress = new DefaultAddress();
    }
    return workAddress;
  }

  public void setWorkAddress(Address workAddress){
    this.workAddress = workAddress;
  }

  

}
