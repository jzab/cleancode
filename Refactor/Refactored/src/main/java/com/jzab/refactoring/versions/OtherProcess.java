package com.jzab.refactoring.versions;

/**
 *
 * @author jzab
 */
public class OtherProcess {

  public void executeController(){
    WindowsController controller = new WindowsController();

    controller.doSame();
    controller.doSomething();
    controller.doAnotherThing();
  }

}
