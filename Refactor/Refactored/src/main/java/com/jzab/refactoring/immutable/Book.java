package com.jzab.refactoring.immutable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zjaramil
 */
public class Book {

  public static int count = 0;

  private static Map<String, Book> objectPool = new HashMap<>();

  private final String title;
  private String author;
  private List<String> chapters;
  private Date publishDate;



  private Book(String title, String author, List<String> chapters, Date publishDate){
    this.title = title;
    this.author = author;
    this.chapters = new ArrayList<>();
    this.chapters.addAll(chapters);
    this.publishDate = new Date(publishDate.getTime());
    count++;
  }

  public String getTitle(){
    return title;
  }

  public String getAuthor(){
    return author;
  }

  public List<String> getChapters(){
    return Collections.unmodifiableList(chapters);
  }

  public Date getPublishDate(){
    return new Date(publishDate.getTime());
  }

  public Book addChapters(List<String> newChapters){
    List<String> list = new ArrayList<>();
    list.addAll(this.chapters);
    list.addAll(newChapters);
    return new Book(this.title, this.author, list, this.publishDate);
  }

  @Override
  public String toString(){
    String chaptersStr = "";
    for (String chapter : chapters){
      chaptersStr += chapter + ", ";
    }
    String dateStr = publishDate.toString();
    return String.format("Book[Title='%s', Author='%s', Chapters='%s', Published='%s']", title, author,
                         chaptersStr, dateStr);
  }
  public static Book create(String title, String author, List<String> chapters, Date publishDate){
    if(objectPool.containsKey(title)){
      return objectPool.get(title);
    }
    else{
      Book book =new Book(title, author, chapters, publishDate);
      objectPool.put(title, book);
      return book;
    }
  }

}
