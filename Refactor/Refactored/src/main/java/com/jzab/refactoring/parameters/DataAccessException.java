package com.jzab.refactoring.parameters;

/**
 *
 * @author jzab
 */
public class DataAccessException extends Exception {

  public DataAccessException(String message, Throwable cause){
    super(message, cause);
  }
  
}
