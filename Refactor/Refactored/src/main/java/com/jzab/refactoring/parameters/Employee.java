/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.parameters;

/**
 *
 * @author jzab
 */
class Employee {

  private String name;
  private String department;
  private String level;
  private int managerId;

  public String getName(){
    return name;
  }

  public void setName(String name){
    this.name = name;
  }

  public String getDepartment(){
    return department;
  }

  public void setDepartment(String department){
    this.department = department;
  }

  public String getLevel(){
    return level;
  }

  public void setLevel(String level){
    this.level = level;
  }

  public int getManagerId(){
    return managerId;
  }

  public void setManagerId(int managerId){
    this.managerId = managerId;
  }

  

}
