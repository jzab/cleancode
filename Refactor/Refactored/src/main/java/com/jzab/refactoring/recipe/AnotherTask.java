package com.jzab.refactoring.recipe;

/**
 *
 * @author zjaramil
 */
class AnotherTask extends ITask{

  @Override
  public void prepare(){
  }
    
  public Object calculate(){
    return "Another";
  }

  public void compute(Object calculation){
    System.out.println("Common execution code");
    System.out.printf("Execute %s task %n", calculation);
  }

  public void finish(){
    System.out.println("Finish task");
    System.out.println("------------------");
  }



}
