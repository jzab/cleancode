/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.concurrent;

import java.util.TimerTask;

/**
 *
 * @author jzab
 */
class ThreadImpl extends TimerTask {

  private int computerNumber;

  public ThreadImpl(){
  }

  @Override
  public void run(){
    computerNumber = (int)(Math.random() * 10);
    System.out.printf("Guess my number (Hack:%d) %n", computerNumber);
  }

  public int getComputerNumber(){
    return computerNumber;
  }

}
