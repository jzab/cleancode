/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.recipe;

/**
 *
 * @author zjaramil
 */
public class Main {

  public static void main(String[] args){
    Task task = new Task();
    task.execute();

    OtherTask oTask = new OtherTask();
    oTask.execute();

    AnotherTask aTask = new AnotherTask();
    aTask.execute();

    //This bad developer didn't follow the order we want
    OtherTask badTask = new OtherTask();
    badTask.execute();
  }

}
