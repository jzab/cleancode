package com.jzab.refactoring.recipe;

/**
 *
 * @author zjaramil
 */
public class Task extends ITask {

  @Override
  public void prepare(){
    System.out.println("Preparing for task");
  }

  @Override
  public Object calculate(){
    return "Task";
  }

  @Override
  public void compute(Object calculation){
    System.out.println("Common execution code");
    System.out.printf("Execute %s task %n", calculation);
  }

  @Override
  public void finish(){
    System.out.println("Finish task");
    System.out.println("------------------");
  }

  

}
