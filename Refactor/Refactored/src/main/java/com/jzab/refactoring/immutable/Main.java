package com.jzab.refactoring.immutable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author zjaramil
 */
public class Main {

  public static void main(String[] args){
    List<String> chapters = new ArrayList<>();
    chapters.add("Prefacio");
    chapters.add("1");
    chapters.add("2");
    chapters.add("3");
    Date date = new Date(1864, 10, 25);

    Book book = Book.create("La vuelta al mundo en 80 dias", "Julio Verne", chapters, date);
    Book book2 = Book.create("La vuelta al mundo en 80 dias", "Julio Verne", chapters, date);
    Book book3 = Book.create("La vuelta al mundo en 80 dias", "Julio Verne", chapters, date);
    System.out.println(Book.count);

    chapters.add("4");
    date.setMonth(1);

    Integer x = Integer.valueOf(0);
    Integer x2 = new Integer(0);
    Integer x3 = Integer.valueOf(0);

    BookExtender a = new BookExtender(book, "A");
    BookExtender b = new BookExtender(book, "B");
    BookExtender c = new BookExtender(book, "C");

    a.start();
    b.start();
    c.start();

    //Change book information to register as my book
    //book.setAuthor("Zabdiel Jaramillo");
    //book.setPublishDate(new Date());
  }

  private static class BookExtender extends Thread {

    private Book book;
    private String prefix;

    private BookExtender(Book book, String prefix){
      this.book = book;
      this.prefix = prefix;
    }

    @Override
    public void run(){

      List<String> chapters = new ArrayList<>();
      for (int i = 1; i <= 5; i++){
        chapters.add(prefix + i);
      }
      Book newBook = book.addChapters(chapters);
      System.out.printf("My Book(%s):%n%s%n", prefix, newBook.toString());
    }

  }

}
