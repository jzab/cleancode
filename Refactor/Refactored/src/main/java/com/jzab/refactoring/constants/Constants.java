package com.jzab.refactoring.constants;

/**
 *
 * @author zjaramil
 */
public class Constants {

  public static void main(String[] args){
    Event e = new DoubleClickEvent();
    processEvent(e);
  }

  private static void processEvent(Event e){
    e.process();
  }

}
