/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jzab.refactoring.recipe;

/**
 *
 * @author jzab
 */
public abstract class ITask {

  public void execute(){
    prepare();
    Object calculation = calculate();
    compute(calculation);
    finish();
  }

  protected abstract Object calculate();

  protected abstract void compute(Object calculation);

  protected abstract void finish();

  protected abstract void prepare();

}
