package com.jzab.refactoring.concurrent;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

/**
 *
 * @author zjaramil
 */
public class GuessGame {

  public void start(){

    ThreadImpl task = new ThreadImpl();
    Timer timer = new Timer();
    timer.scheduleAtFixedRate(task, 0, 3000);

    Scanner scanner = new Scanner(System.in);

    int userNumber = -1;
    do{
      System.out.println("Try:");
      String line = scanner.nextLine();

      if(Pattern.matches("\\d", line)){
        userNumber = Integer.parseInt(line);
      }
      else{
        System.out.println("Write only numbers");
      }
    }
    while(task.getComputerNumber() != userNumber);

    System.out.println("You win");

  }


  public static void main(String[] args){
    new GuessGame().start();
  }


}
